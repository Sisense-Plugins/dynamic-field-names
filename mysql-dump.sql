-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: jobcandidates
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `candidatecustomfields`
--

DROP TABLE IF EXISTS `candidatecustomfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidatecustomfields` (
  `﻿companyId` int(11) DEFAULT NULL,
  `candidateId` int(11) DEFAULT NULL,
  `customFieldId` int(11) DEFAULT NULL,
  `Value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidatecustomfields`
--

LOCK TABLES `candidatecustomfields` WRITE;
/*!40000 ALTER TABLE `candidatecustomfields` DISABLE KEYS */;
INSERT INTO `candidatecustomfields` VALUES (1,10,1,'New York'),(1,10,2,'5'),(1,10,3,'Yes'),(1,11,1,'Arizona'),(1,11,2,'4'),(1,11,3,'No'),(1,12,1,'Arizona'),(1,12,2,'6'),(1,12,3,'No'),(1,13,1,'New York'),(1,13,2,'1'),(1,13,3,'Yes'),(1,14,1,'New York'),(1,14,2,'2'),(1,14,3,'No'),(2,15,4,'Yes'),(2,15,5,'No'),(2,16,4,'Yes'),(2,16,5,'Yes');
/*!40000 ALTER TABLE `candidatecustomfields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidates`
--

DROP TABLE IF EXISTS `candidates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidates` (
  `﻿companyId` int(11) DEFAULT NULL,
  `candidateId` int(11) DEFAULT NULL,
  `name` text,
  `source` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidates`
--

LOCK TABLES `candidates` WRITE;
/*!40000 ALTER TABLE `candidates` DISABLE KEYS */;
INSERT INTO `candidates` VALUES (1,10,'Danielle','LinkedIn'),(1,11,'Terry','LinkedIn'),(1,12,'Brian','Monster.com'),(1,13,'Jeremy','Referral'),(1,14,'Jessica','LinkedIn'),(2,15,'Amy','Monster.com'),(2,16,'Lisa','LinkedIn');
/*!40000 ALTER TABLE `candidates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `﻿companyId` bigint(20) DEFAULT NULL,
  `companyName` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'Sisense'),(2,'Villanova University');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customfields`
--

DROP TABLE IF EXISTS `customfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customfields` (
  `﻿companyId` int(11) DEFAULT NULL,
  `customFieldId` int(11) DEFAULT NULL,
  `fieldName` text,
  `type` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customfields`
--

LOCK TABLES `customfields` WRITE;
/*!40000 ALTER TABLE `customfields` DISABLE KEYS */;
INSERT INTO `customfields` VALUES (1,1,'Site','Candidate'),(1,2,'YearsExperience','Candidate'),(1,3,'MastersDegree','Candidate'),(2,4,'isAlumni','Candidate'),(2,5,'PHD','Candidate');
/*!40000 ALTER TABLE `customfields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `v_candidate_custom_fields`
--

DROP TABLE IF EXISTS `v_candidate_custom_fields`;
/*!50001 DROP VIEW IF EXISTS `v_candidate_custom_fields`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `v_candidate_custom_fields` AS SELECT 
 1 AS `﻿companyId`,
 1 AS `candidateId`,
 1 AS `customFieldId`,
 1 AS `fieldName`,
 1 AS `Value`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping routines for database 'jobcandidates'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_candidate_custom_fields` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_candidate_custom_fields`()
BEGIN
		
	SET @rn1 = 0;
    SET @rn2 = '';
	SET @pk1 = '';
	SET @sa1  ='';
    SET @customFieldPrefix = 'c';

	/* 	Create a temporary table to store the custom field ranking	*/
	DROP TABLE IF EXISTS `tmp_rank`;
	CREATE TEMPORARY TABLE IF NOT EXISTS `tmp_rank` (
		`companyId` BIGINT UNSIGNED NOT NULL,
		`customFieldId` BIGINT UNSIGNED NOT NULL,
		`newfieldId` VARCHAR(100) NOT NULL    
	) ENGINE=MEMORY;

	/* 	Populate the ranking temporary table */
	INSERT INTO `tmp_rank`
		select `t`.`﻿companyId`,  `t`.`customFieldId`, `t`.`sharedCustomFieldId`
		from (
			select `f`.`﻿companyId`,  `f`.`customFieldId`
				, @rn1 := if(@pk1=`f`.`﻿companyId`, if(@sal= `f`.`customFieldId`, @rn1, @rn1+1),1) as `sharedCustomFieldInt`
                , @rn2 :=  concat(@customFieldPrefix, CAST( @rn1 as CHAR(100) ) ) as `sharedCustomFieldId`
				, @pk1 := `f`.`﻿companyId`
				, @sal :=  `f`.`customFieldId`
			from  `jobcandidates`.`customfields` as `f`
			GROUP BY  `f`.`﻿companyId`,  `f`.`customFieldId`
		) as t;
		
		
	/* 	Create a temporary table to store the custom fields	*/
	DROP TABLE IF EXISTS `tmp`;
	CREATE TEMPORARY TABLE IF NOT EXISTS `tmp` (
		`companyId` BIGINT UNSIGNED NOT NULL,
		`candidateId` BIGINT UNSIGNED NOT NULL,
		`fieldId` VARCHAR(100) NOT NULL,
		`fieldName` VARCHAR(520) NOT NULL,
		`fieldValue` VARCHAR(520) NOT NULL
	) ENGINE=MEMORY;

	/* 	Populate the temporary table */
	INSERT INTO `tmp`
		SELECT `t`.`companyId`, `t`.`candidateId`, `t`.`sharedFieldId`, `t`.`fieldName`, `t`.`Value`
		FROM (
			select `f`.`﻿companyId` as `companyId`
				,`c`.`candidateId` as `candidateId`
				,`f`.`customFieldId` as `originalCustomFieldId`
				,`r`.`newFieldId` as `sharedFieldId`
				,`f`.`fieldName` as `fieldName`
				,`c`.`Value` as `Value`			
			from `jobcandidates`.`candidatecustomfields` `c` 
				, `jobcandidates`.`customfields` `f`
				, tmp_rank `r`
			where (`f`.`customFieldId` = `c`.`customFieldId`)
				and (`f`.`﻿companyId` = `c`.`﻿companyId`) 
				and (`f`.`type` = 'candidate')
				and (`f`.`﻿companyId` = `r`.`companyId`) 
				and (`f`.`customFieldId` = `r`.`customFieldId`)
		) t;
			
	/* Creat SQL to pivot the custom fields into columns	*/
	SET @sql = NULL;
	SELECT
	  GROUP_CONCAT(DISTINCT
		CONCAT(
		  'MAX(IF(t.`fieldId` = ''',
		  `t`.fieldId,
		  ''', t.`fieldValue`, NULL)) AS ',
		  `t`.fieldId
		)
	  ) INTO @sql
	FROM tmp as `t`;
	SET @sql = CONCAT('select `t`.`companyId`, `t`.`candidateId`, ', @sql, ' from tmp as `t` group by  `t`.`companyId`, `t`.`candidateId`');


	/* 	Run the SQL 	*/

	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_candidate_custom_field_labels` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_candidate_custom_field_labels`()
BEGIN
	SET @rn1 = 0;
    SET @rn2 = '';
	SET @pk1 = '';
	SET @sa1  ='';
    SET @customFieldPrefix = 'c';

	select `t`.`﻿companyId`,  `t`.`customFieldId`, `t`.`sharedCustomFieldId`, `t`.`fieldName`
	from (
		select `f`.`﻿companyId`,  `f`.`customFieldId`, `f`.`fieldName`
				, @rn1 := if(@pk1=`f`.`﻿companyId`, if(@sal= `f`.`customFieldId`, @rn1, @rn1+1),1) as `sharedCustomFieldInt`
                , @rn2 :=  concat(@customFieldPrefix, CAST( @rn1 as CHAR(100) ) ) as `sharedCustomFieldId`
				, @pk1 := `f`.`﻿companyId`
				, @sal :=  `f`.`customFieldId`
		from  `jobcandidates`.`customfields` as `f`
		where `f`.`type` = 'candidate'
		GROUP BY  `f`.`﻿companyId`,  `f`.`customFieldId`
	) as t;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_custom_fields` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_custom_fields`()
BEGIN
	SET @sql = NULL;
	SELECT
	  GROUP_CONCAT(DISTINCT
		CONCAT(
		  'MAX(IF(c.`customFieldId` = ''',
		  c.customFieldId,
		  ''', c.`Value`, NULL)) AS c_',
		  c.customFieldId
		)
	  ) INTO @sql
	FROM jobcandidates.v_candidate_custom_fields as c;
	SET @sql = CONCAT('select c.`﻿companyId`, c.`candidateId`, ', @sql, ' from jobcandidates.v_candidate_custom_fields as c group by c.`﻿companyId`, c.`candidateId`');


	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `v_candidate_custom_fields`
--

/*!50001 DROP VIEW IF EXISTS `v_candidate_custom_fields`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `v_candidate_custom_fields` AS select `f`.`﻿companyId` AS `﻿companyId`,`c`.`candidateId` AS `candidateId`,`f`.`customFieldId` AS `customFieldId`,`f`.`fieldName` AS `fieldName`,`c`.`Value` AS `Value` from (`candidatecustomfields` `c` join `customfields` `f`) where ((`f`.`customFieldId` = `c`.`customFieldId`) and (`f`.`﻿companyId` = `c`.`﻿companyId`) and (`f`.`type` = 'candidate')) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-30 11:39:57
