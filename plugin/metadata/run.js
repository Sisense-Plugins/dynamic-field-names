prism.run([ function () {
  
  //  Define the template for metadata translation
  var datasourceAliasing = {
    "tables": {},
    "formulas": {},
    "hierarchies": {},
    "titles": {},
    "columns": {
      "Candidate Custom Fields": {}
    }
  };

  //  Define which elasticube(s) this applies to, or use 'all' to run for every elasticube
  var secureElasticubes = 'all';    //  ["elasticube1", "elasticube2", "elasticube3"];

  //  Define JAQL query to fetch the translations
  var queryPayload = {
    "datasource": '',
    "format": "json",
    "metadata":[
      {
        "jaql": {
                "table": "CustomFields",
                "column": "field_id",
                "dim": "[CustomFields.field_id]",
                "datatype": "text",
          }
      },{
          "jaql": {
            "table": "CustomFields",
            "column": "fieldName",
            "dim": "[CustomFields.fieldName]",
            "datatype": "text",
            "title": "fieldName"
        }
      }
    ]
  }

  //  Handler function for when deciding what metadata to display
  var datasourceProvider = function (datasource, resolve, reject) {

    //  Get the Elasticube info, assign to the payload
    var ecname = (typeof datasource === "string") ? datasource : datasource.title;

    //  Is it ok to run for this elasticube?
    var isValid = (typeof secureElasticubes === "string") ? (secureElasticubes==="all") : ( secureElasticubes.indexOf(ecname) >=0 );
    if (isValid){
  
      //  get Angular's $http service for ajax requests
      var $http = prism.$injector.get('$http');

      //  Set the query payload's data source
      queryPayload.datasource = datasource;

      //  Define the query url
      var queryUrl = '/api/datasources/' + ecname + '/jaql';

      //  Make API call to get the translations
      $http.post(queryUrl, queryPayload)
        .then(function (response) {

          //  Get the translations from the response
          var translations = $$get(response, 'data.values', []);

          //  Was the response valid?
          if (translations && translations.length) {

            //  Loop through each translation
            translations.forEach(function(row){

              //  Get the column name (ec name) and the label to use
              var columnId = row[0].text,
                columLabel = row[1].text;

              //  Save to the datasource alias object
              datasourceAliasing.columns["Candidate Custom Fields"][columnId] = columLabel;
              datasourceAliasing.titles[columnId] = columLabel;
            })
            
            return resolve(datasourceAliasing);
          } else {

            //  Invalid response
            return resolve();
          }
        })
        .catch(reject);
        
    } else {
      return resolve();
    }
  };

  //  Use event handler to register metadata handler function
  prism.on("beforealiascontextinit", function (ev, args) {
    var timeout = 5000;
    args.register(datasourceProvider, timeout);
  });

}]);